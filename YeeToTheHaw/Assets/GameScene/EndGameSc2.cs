﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameSc2 : MonoBehaviour
{

    public LassoMech mech;
    public GameObject UiOver;
    public GameObject UiSuccess;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mech.Victory == 2)
        {
            //Debug.Log("Cheh");
            UiOver.SetActive(true);
            Time.timeScale = 0;

        }
        else if (mech.Victory == 1)
        {
            //Debug.Log("Cheh");
            UiSuccess.SetActive(true);
            Time.timeScale = 0;
        }

    }
}
