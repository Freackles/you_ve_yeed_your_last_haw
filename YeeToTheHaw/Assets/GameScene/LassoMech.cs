﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using OpenCVForUnityExample;

public class LassoMech : MonoBehaviour
{
    public float TimeLimit = 5f;
    private float timeCount = 0f;
    private int poingCompte = 0;
    public int PoingLimite = 20;
    public HandPoseEstimationExample handTracking;
    public int Victory = 0;
    private bool trackingBefore;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Animator>().enabled = false;
        trackingBefore = handTracking.poingFermer;
    }

    // Update is called once per frame
    void Update()
    {
        //comptage du nombre de fois où le joueur a fermer les poings
        if ((handTracking.poingFermer == true) && (trackingBefore == false))
        {
            poingCompte += 1;
            trackingBefore = handTracking.poingFermer;
            GetComponent<Animator>().enabled = true;
        }
        else if (handTracking.poingFermer == false)
        {
            //transform.localScale = new Vector3(200f, 200f, transform.localScale.z);
            GetComponent<Animator>().enabled = false;
            trackingBefore = handTracking.poingFermer;
        }

        timeCount += Time.deltaTime;

        //condition de victoire

        if (poingCompte == PoingLimite)
        {
            Victory = 1;
        }
        else if ((poingCompte < PoingLimite) && (TimeLimit == (timeCount + 20)))
        {
            Victory = 2;
        }
    }
}
